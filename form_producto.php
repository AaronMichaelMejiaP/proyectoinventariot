
<?php 
session_start();
$vIdProducto = $_GET["id"];
if(!isset($_SESSION["Usuario"])) {
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./includes/header.php")?>
</head>
<body id="page-top">
  <div id="wrapper">
    <?php include("./includes/slidebar.php")?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php include("./includes/navbar.php")?>
        <div class="container-fluid">
          <section>
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
          </section>
          <!-- CONTENIDO PAGINA -->
          <section>
            <h3>Producto</h3>
          </section>
          <section>
            <div class="card">
              <div class="card-body">
                  <form action="dao/operaciones/producto_crud.php" method="POST">
                  <?php
                  include_once 'dao/conexion/conexiondb.php';
                  include_once "dao/objects/producto.php";
                  include_once "dao/objects/proveedor.php";
                  include_once "dao/objects/marca.php";
                  include_once "dao/objects/tipo.php";
                  include_once "dao/objects/estado.php";
                  $database = new Database();
                  $db = $database->getConnection();

                  $objProducto = new Producto($db);
                  $objProducto->idProducto=$vIdProducto;
              
                  $stmt = $objProducto->getById();
                  $num = $stmt->rowCount();
              
                  if($num==1){
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    extract($row);
                    $vIdProveedorProducto = $idProveedor;
                    $vIdTipoProducto = $idTipo;
                    $vIdMarcaProducto = $idMarca;
                    $vIdEstado = $idEstado;
                    ?>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                          <label>Producto</label>
                          <input type="text" class="form-control" name="producto" value="<?php echo $producto ?>" required>
                          <input type="hidden" name="idProducto" value="<?php echo $idProducto ?>">
                        </div>
                        <div class="col-md-3 mb-3">
                          <label>Proveedor</label>
                          <select class="custom-select" name="idProveedor" required>
                              <option selected disabled>Selecciona una opcion...</option>
                              <?php
                                $objProveedor = new Proveedor($db);
                                $stmt = $objProveedor->get();
                                $num = $stmt->rowCount();
                                if($num != 0){
                                    $indice = 0;
                                    $ComboSeleccionado="";
                                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                    extract($row);
                                        if($vIdProveedorProducto == $idProveedor){
                                            $ComboSeleccionado = "selected";
                                        }
                                        else{
                                            $ComboSeleccionado = "";
                                        }
                                    
                              ?>
                                <option value="<?php echo $idProveedor?>" <?php echo $ComboSeleccionado?>><?php echo $proveedor?></option>
                              <?php
                                    }
                                }
                              ?>
                          </select>
                        </div>
                        <div class="col-md-3 mb-3">
                          <label>Codigo</label>
                          <input type="text" class="form-control" name="codigo" value="<?php echo $codigo ?>" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label>Precio Compra</label>
                          <input type="number" min="0" step="any" class="form-control" name="precioCompra" value="<?php echo $precioCompra ?>" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Precio Venta</label>
                          <input type="number" min="0" step="any" class="form-control" name="precioVenta" value="<?php echo $precioVenta ?>" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Cantidad</label>
                          <input type="number" min="0" step="any" class="form-control" name="cantidad" value="<?php echo $cantidad ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                      
                      <label for="exampleInputEmail1">Ingrese posibles precios</label>
                      <ul class="list-group">
                        <div id="preciosIngresados">
                          <input type="hidden" name="preciosRegistro" value="<?php echo $preciosParaVenta;?>">
                        <?php
                          $vPrecioOptenidoString="";
                          for ($x = 0; $x < strlen($preciosParaVenta) ; $x++) {
                            if($preciosParaVenta[$x] != ";"){
                              $vPrecioOptenidoString = $vPrecioOptenidoString.$preciosParaVenta[$x];
                            }
                            else{
                              ?>
                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                <?php echo $vPrecioOptenidoString ?>
                                <button type="button" class="btn btn-danger btn-sm" onclick="eliminarPrecioRegistrado('<?php echo $preciosParaVenta?>' , '<?php echo $vPrecioOptenidoString?>')" disabled>
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                              </li>
                              <?php
                              $vPrecioOptenidoString = "";
                            }
                          }
                        ?>
                        </div>
                      </ul>
                      <div id="contenidoPrincipalPrecios">
                        <div class="d-flex justify-content-end">
                          <button type="button" class="btn btn-secondary btn-sm btn-block" onclick="editarPreciosRegistrados('<?php echo $preciosParaVenta;?>')">Editar precios</button>
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label>Marca</label>
                          <select class="custom-select" name="idMarca" required>
                              <option selected disabled>Selecciona una opcion...</option>
                              <?php
                                $objMarca = new Marca($db);
                                $stmt = $objMarca->get();
                                $num = $stmt->rowCount();
                                if($num != 0){
                                    $indice = 0;
                                    $ComboSeleccionado="";
                                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                    extract($row);
                                        if($vIdMarcaProducto == $idMarca){
                                            $ComboSeleccionado = "selected";
                                        }
                                        else{
                                            $ComboSeleccionado = "";
                                        }
                                    
                              ?>
                                <option value="<?php echo $idProveedor?>" <?php echo $ComboSeleccionado?>><?php echo $marca?></option>
                              <?php
                                    }
                                }
                              ?>
                          </select>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Tipo</label>
                          <select class="custom-select" name="idTipo" required>
                              <option selected disabled>Selecciona una opcion...</option>
                              <?php
                                $objTipo = new Tipo($db);
                                $stmt = $objTipo->get();
                                $num = $stmt->rowCount();
                                if($num != 0){
                                    $indice = 0;
                                    $ComboSeleccionado="";
                                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                    extract($row);
                                        if($vIdTipoProducto == $idTipo){
                                            $ComboSeleccionado = "selected";
                                        }
                                        else{
                                            $ComboSeleccionado = "";
                                        }
                                    
                              ?>
                                <option value="<?php echo $idTipo?>" <?php echo $ComboSeleccionado?>><?php echo $tipo?></option>
                              <?php
                                    }
                                }
                              ?>
                          </select>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Estado Producto</label>
                          <select class="custom-select" name="idEstado" required>
                            <option selected disabled>Selecciona una opcion...</option>
                            <?php
                              $objTipo = new Estado($db);
                              $stmt = $objTipo->get();
                              $num = $stmt->rowCount();
                              if($num != 0){
                                  $indice = 0;
                                  $ComboSeleccionado="";
                                  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                  extract($row);
                                      if($vIdEstado == $idEstado){
                                          $ComboSeleccionado = "selected";
                                      }
                                      else{
                                          $ComboSeleccionado = "";
                                      }
                            ?>
                              <option value="<?php echo $idEstado?>" <?php echo $ComboSeleccionado?>><?php echo $estado?></option>
                            <?php
                                  }
                              }
                            ?>
                          </select>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-warning" type="submit" name="producto_editar">Editar</button>
                    </div>
                    <?php
                  }
                  else{
                    ?>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                          <label>Producto</label>
                          <input type="text" class="form-control" name="producto" required>
                        </div>
                        <div class="col-md-3 mb-3">
                          <label>Proveedor</label>
                          <select class="custom-select" name="idProveedor" required>
                              <option selected disabled>Selecciona una opcion...</option>
                              <?php
                                $objProveedor = new Proveedor($db);
                                $stmt = $objProveedor->get();
                                $num = $stmt->rowCount();
                                if($num != 0){
                                    $indice = 0;
                                    $ComboSeleccionado="";
                                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                    extract($row);
                              ?>
                                <option value="<?php echo $idProveedor?>" <?php echo $ComboSeleccionado?>><?php echo $proveedor?></option>
                              <?php
                                    }
                                }
                              ?>
                          </select>
                        </div>
                        <div class="col-md-3 mb-3">
                          <label>Codigo</label>
                          <input type="text" class="form-control" name="codigo" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label>Precio Compra</label>
                          <input type="number" min="0" step="any" class="form-control" name="precioCompra" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Precio Venta</label>
                          <input type="number" min="0" step="any" class="form-control" name="precioVenta" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Cantidad</label>
                          <input type="number" min="0" step="any" class="form-control" name="cantidad" required>
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Ingrese posibles precios</label>
                      <ul class="list-group">
                        <div id="preciosIngresados">
                          <input type="hidden" name="preciosRegistro" value="">
                        </div>
                      </ul>
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Ingresa un precio" id="precioAgregado">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button" onclick="registrarPrecio()">
                            <i class="fas fa-plus"></i>  
                            Agregar
                            </button>
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                          <label>Marca</label>
                          <select class="custom-select" name="idMarca" required>
                              <option selected disabled>Selecciona una opcion...</option>
                              <?php
                                $objMarca = new Marca($db);
                                $stmt = $objMarca->get();
                                $num = $stmt->rowCount();
                                if($num != 0){
                                    $indice = 0;
                                    $ComboSeleccionado="";
                                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                    extract($row);
                                    
                              ?>
                                <option value="<?php echo $idProveedor?>" <?php echo $ComboSeleccionado?>><?php echo $marca?></option>
                              <?php
                                    }
                                }
                              ?>
                          </select>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label>Tipo</label>
                          <select class="custom-select" name="idTipo" required>
                              <option selected disabled>Selecciona una opcion...</option>
                              <?php
                                $objTipo = new Tipo($db);
                                $stmt = $objTipo->get();
                                $num = $stmt->rowCount();
                                if($num != 0){
                                    $indice = 0;
                                    $ComboSeleccionado="";
                                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                    extract($row);
                              ?>
                                <option value="<?php echo $idTipo?>" <?php echo $ComboSeleccionado?>><?php echo $tipo?></option>
                              <?php
                                    }
                                }
                              ?>
                          </select>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-success" type="submit" name="producto_nuevo">Agregar</button>
                    </div>
                    <?php
                  }
                  ?>
                    
                  </form>
              </div>
            </div>
          </section>
         
        </div>
      </div>
      <?php include("./includes/footer.php")?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include("./includes/scripts.php")?>

  <script src="operaciones/producto.js"></script>
</body>

</html>
