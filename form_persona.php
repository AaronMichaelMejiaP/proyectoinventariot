
<?php 
session_start();
$vId = $_GET["id"];
if(!isset($_SESSION["Usuario"])) {
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./includes/header.php")?>
</head>
<body id="page-top">
  <div id="wrapper">
    <?php include("./includes/slidebar.php")?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php include("./includes/navbar.php")?>
        <div class="container-fluid">
          <section>
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
          </section>
          <!-- CONTENIDO PAGINA -->
          <section>
            <h3>Persona</h3>
          </section>
          <section>
            <div class="card">
              <div class="card-body">
                  <form action="dao/operaciones/persona_crud.php" method="POST">
                  <?php
                  include_once 'dao/conexion/conexiondb.php';
                  include_once "dao/objects/persona.php";
                  $database = new Database();
                  $db = $database->getConnection();

                  $objPersona = new Persona($db);
                  $objPersona->idPersona=$vId;
              
                  $stmt = $objPersona->getById();
                  $num = $stmt->rowCount();
                  if($num==1){
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    extract($row);
                    ?>
                    <h6>Datos personales</h6>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                          <label>Primer Nombre</label>
                          <input type="text" class="form-control" name="primerNombre" value="<?= $primerNombre?>" required>
                          <input type="hidden" name="idPersona" value="<?= $idPersona;?>">
                        </div>
                        <div class="col-md-6 mb-3">
                          <label>Segundo Nombre</label>
                          <input type="text" class="form-control" name="segundoNombre" value="<?= $segundoNombre?>" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                          <label>Primer Apellido</label>
                          <input type="text" class="form-control" name="primerApellido" value="<?= $primerApellido?>" required>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label>Segundo Apellido</label>
                          <input type="text" class="form-control" name="segundoApellido" value="<?= $segundoApellido?>" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                          <label>Carnet</label>
                          <div class="input-group">
                            <input type="number" min="0" name="ci" value="<?= $ci?>" class="form-control">
                            <select class="custom-select" name="extension" required>
                              <option selected disabled>Extension...</option>
                              <?php
                              $opcionesExpedido = array("LP","CBBA","SC","OTRO");
                              foreach($opcionesExpedido as $data){
                                if($data == $expedido){
                                  $ComboSeleccionado = "selected";
                                }
                                else{
                                  $ComboSeleccionado = "";
                                }
                                ?>
                                <option value="<?php $data?>" <?= $ComboSeleccionado?>><?php echo $data;?></option>
                                <?php
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Correo</label>
                          <input type="email" class="form-control" name="correo" value="<?= $correo?>" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Fecha de nacimiento</label>
                          <input type="date" class="form-control" name="fechaNacimiento" value="<?= $fechaNacimiento?>" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label>Telefono</label>
                          <input type="number" min="0" class="form-control" name="telefono" value="<?= $telefono?>" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Celular</label>
                          <input type="number" min="0" class="form-control" name="celular" value="<?= $celular?>" required>
                        </div>
                    </div>
                    <hr>
                    <h6>Direccion</h6>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label>Zona</label>
                          <input type="text" class="form-control" name="zona" value="<?= $zona?>" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Calle</label>
                          <input type="text" class="form-control" name="calle" value="<?= $calle?>" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Nro.</label>
                          <input type="text" class="form-control" name="numeroCasa" value="<?= $numeroCasa?>" required>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-warning" type="submit" name="persona_editar">Editar</button>
                    </div>
                    <?php
                  }
                  else{
                    ?>
                    <h6>Datos personales</h6>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                          <label>Primer Nombre</label>
                          <input type="text" class="form-control" name="primerNombre" required>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label>Segundo Nombre</label>
                          <input type="text" class="form-control" name="segundoNombre">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                          <label>Primer Apellido</label>
                          <input type="text" class="form-control" name="primerApellido" required>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label>Segundo Apellido</label>
                          <input type="text" class="form-control" name="segundoApellido">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                          <label>Carnet</label>
                          <div class="input-group">
                            <input type="number" min="0" name="ci" class="form-control">
                            <select class="custom-select" name="extension" required>
                              <option selected disabled>Extension...</option>
                              <option value="CBBA">CBBA</option>
                              <option value="LP">LP</option>
                              <option value="STC">STC</option>
                              <option value="">Otro</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Correo</label>
                          <input type="email" class="form-control" name="correo" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Fecha de nacimiento</label>
                          <input type="date" class="form-control" name="fechaNacimiento" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label>Telefono</label>
                          <input type="number" min="0" class="form-control" name="telefono" value="0" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Celular</label>
                          <input type="number" min="0" class="form-control" name="celular" value="0" required>
                        </div>
                    </div>
                    <hr>
                    <h6>Direccion</h6>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label>Zona</label>
                          <input type="text" class="form-control" name="zona" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Calle</label>
                          <input type="text" class="form-control" name="calle" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Nro.</label>
                          <input type="text" class="form-control" name="numeroCasa" required>
                        </div>
                    </div>
                    
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-success" type="submit" name="persona_nuevo">Agregar</button>
                    </div>
                    <?php
                  }
                  ?>
                    
                  </form>
              </div>
            </div>
          </section>
         
        </div>
      </div>
      <?php include("./includes/footer.php")?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include("./includes/scripts.php")?>

  <script src="operaciones/producto.js"></script>
</body>

</html>
