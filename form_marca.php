
<?php 
session_start();
$vId = $_GET["id"];
if(!isset($_SESSION["Usuario"])) {
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./includes/header.php")?>
</head>
<body id="page-top">
  <div id="wrapper">
    <?php include("./includes/slidebar.php")?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php include("./includes/navbar.php")?>
        <div class="container-fluid">
          <section>
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
          </section>
          <!-- CONTENIDO PAGINA -->
          <section>
            <h3>Marca</h3>
          </section>
          <section>
            <div class="card">
              <div class="card-body">
                <form action="dao/operaciones/marca_crud.php" method="POST">
                  <div class="form-group">
                    <label for="">Marca</label>
                    <input type="text" class="form-control" name="marca" placeholder="Ingrese una marca...">
                  </div>
                  <div class="form-group">
                    <label for="">Descripcion</label>
                    <input type="text" class="form-control" name="descripcion" placeholder="Ingrese una descripcion...">
                  </div>
                  <div class="d-flex justify-content-end">
                        <button class="btn btn-success" type="submit" name="marca_nuevo">Crear</button>
                  </div>
                </form>
              </div>
            </div>
          </section>
         
        </div>
      </div>
      <?php include("./includes/footer.php")?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include("./includes/scripts.php")?>

  <script src="operaciones/producto.js"></script>
</body>

</html>
