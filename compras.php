<?php 
include_once "./dao/conexion/conexiondb.php";
include_once "./dao/objects/producto.php";
include_once "./dao/objects/cliente.php";
session_start();
if(!isset($_SESSION["Usuario"])) {
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./includes/header.php")?>
</head>
<body id="page-top">
  <div id="wrapper">
    <?php include("./includes/slidebar.php")?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php include("./includes/navbar.php")?>
        <div class="container-fluid">
          <section>
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
          </section>
          <!-- CONTENIDO PAGINA -->
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-body">
                  <?php 
                  
                  
                  $database = new Database();
                  $db = $database->getConnection();
                  if(isset($_SESSION["ListaProductos"]) && count(json_decode($_SESSION["ListaProductos"])) > 0) {
                    $ListaProductosSesion = json_decode($_SESSION["ListaProductos"]);
                    $TotalParaPagar=0;
                    foreach($ListaProductosSesion as $data){
                      json_decode($data->idProducto);
                      json_decode($data->precio);
                      json_decode($data->cantidad);

                      $obj = new Producto($db);
                      $obj->idProducto=json_decode($data->idProducto);

                      $stmt = $obj->getById();
                      $num = $stmt->rowCount();

                      if($num==1){
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        extract($row);
                        $TotalPorProducto = intval(json_decode($data->cantidad))*intval(json_decode($data->precio));
                        $TotalParaPagar = $TotalParaPagar + $TotalPorProducto;
                        ?>
                        <ul class="list-group">
                          <li class="list-group-item">
                            <div class="row">
                              <div class="col-md-6">
                                <strong><h5><?php echo $producto ?></h5></strong>
                              </div>
                              <div class="col-md-6 d-flex justify-content-end">
                                <a class="btn btn-danger rounded-circle" href="dao/operaciones/elimina_producto.php?id=<?php echo json_decode($data->idProducto)?>">
                                  <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                              </div>
                            </div>
                            <hr>
                            <strong><h6>Detalle</h6></strong>
                            <div class="row">
                              <div class="col-md-6">
                                <strong>Precio:</strong>
                              </div>
                              <div class="col-md-6 d-flex justify-content-end">
                                <?php echo json_decode($data->precio);?> Bs.
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <strong>Cantidad:</strong>
                              </div>
                              <div class="col-md-6 d-flex justify-content-end">
                                <?php echo json_decode($data->cantidad);?>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <strong>Total:</strong>
                              </div>
                              <div class="col-md-6 d-flex justify-content-end">
                                <?php echo $TotalPorProducto; ?> Bs.
                              </div>
                            </div>
                          </li>
                        </ul>
                        <?php
                      }
                    }
                    ?>
                    <div class="card p-3">
                      <div class="row">
                        <div class="col-md-6">
                          TOTAL A PAGAR:
                        </div>
                        <div class="col-md-6 d-flex justify-content-end">
                          <?php echo $TotalParaPagar ?> Bs.
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6 d-flex justify-content-end">
                          <button class="btn btn-success" data-toggle="modal" data-target="#modalCompra">Comprar</button>
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                  else{
                    ?>
                    <div class="alert alert-warning" role="alert">
                      <h4 class="alert-heading">No tiene compras registradas.</h4>
                    </div>
                    <?php
                  }
                  ?>
                </div>
              </div> 
            </div>
            <div class="col-md-6">
            </div>
          </div>
          <!-- Modal -->
          <div class="modal fade" id="modalCompra" tabindex="-1" aria-labelledby="modalCompraLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="modalCompraLabel">Compra</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form class="user" action="dao/operaciones/compra_producto.php" method="POST">
                  <div class="modal-body">
                    <div class="form-group">
                      <input type="hidden" name="total" value="<?php echo $TotalParaPagar?>">
                      <label for="exampleFormControlInput1">Total a pagar:</label>
                      <input type="text" class="form-control" name="totalModificado" value="<?php echo $TotalParaPagar?>" required>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                          <label>Cliente:</label>
                          <div class="input-group mb-3">
                            <select class="custom-select" name="identificador" required>
                                <option selected disabled>Seleccione una opcion...</option>
                                <?php
                                $objCliente = new Cliente($db);
                                $stmt = $objCliente->get();
                                $num = $stmt->rowCount();
                                if($num != 0){
                                  $indice = 0;
                                  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                  extract($row);
                                  $indice = $indice + 1;
                                  ?>
                                  <option value="<?php echo $ci?>"><?php echo $nombre." ".$apellido;?></option>
                                  <?php
                                  }
                                }
                                ?>
                            </select>
                            <a class="input-group-text btn btn-success" href="form_cliente.php">Agregar nuego cliente</a>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Seleccione Tipo Compra:</label>
                      <select class="form-control" name="tipo" required>
                        <option disabled selected>Seleccione una opcion.</option>
                        <option value="1">Compra</option>
                        <option value="2">Cotizacion</option>
                      </select>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" name="realizando_compra" class="btn btn-success" value="Confirmar">
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("./includes/footer.php")?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include("./includes/scripts.php")?>
</body>

</html>
