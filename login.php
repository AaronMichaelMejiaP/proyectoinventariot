<?php session_start();?>

<!DOCTYPE html>
<html lang="en">

<head>

  <?php include("./includes/header.php")?>

</head>

<body>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Bienvenido</h1>
              </div>
              <form class="user" action="dao/login.php" method="POST">
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="usuario" placeholder="Ingresa tu usuario...">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-user" name="pass" placeholder="Ingresa tu contraseña">
                </div>
                <input type="submit" name="verificando_login" class="btn btn-primary btn-user btn-block" value="Ingresar">
              </form>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
