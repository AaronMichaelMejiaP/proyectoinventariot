
<?php 
include_once 'dao/conexion/conexiondb.php';
include_once "dao/objects/ciudad.php";
include_once "dao/objects/reporte.php";
session_start();
$vIdCiudad = $_GET["idCiudad"];
if(!isset($_SESSION["Usuario"])) {
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./includes/header.php")?>
</head>
<body id="page-top">
  <div id="wrapper">
    <?php include("./includes/slidebar.php")?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php include("./includes/navbar.php")?>
        <div class="container-fluid">
          <section>
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
          </section>
          <!-- CONTENIDO PAGINA -->
          <section>
            <h3>Reporte ventas por ciudad</h3>
            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-4">
                <div class="card">
                  <div class="card-body">
                    Ciudad
                    <canvas id="GraficaDetalleCiudad" width="400" height="400"></canvas>
                  </div>
                </div>
              </div>
              <div class="col-md-4"></div>
            </div>
          </section>
          <section>
            <div class="card mb-5">
              <div class="card-body">
                <div class="row">
                <?php
                  $database = new Database();
                  $db = $database->getConnection();
                  $reporte = new Reporte($db);
                  $stmt = $reporte->obtieneVentasPorRegion();
                  $num = $stmt->rowCount();
                  if($num>0){
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                        extract($row);
                        ?>
                        <div class="col-md-6">
                          <div class="card">
                              <div class="card-body">
                                  <h5 class="card-title"><?php echo $ciudad?></h5>
                                  <h6 class="card-subtitle mb-2 text-muted">Ventas: <?php echo $ventas?></h6>
                                  <a href="reporte_ventas_ciudad.php?idCiudad=<?php echo $idCiudad?>" class="card-link">Ver Detalle</a>
                              </div>
                          </div>
                        </div>
                        <?php
                    }
                  }
                ?>
                </div>
              </div>
            </div>
            <?php 
            if($vIdCiudad != 0){
            ?>
            <div class="card">
                <div class="card-header">
                    <?php 
                    $obj = new Ciudad($db);
                    $obj->idCiudad=$vIdCiudad;
                    $stmt = $obj->getById();
                    $num = $stmt->rowCount();
                    if($num != 0){
                      $row = $stmt->fetch(PDO::FETCH_ASSOC);
                      extract($row);
                      ?>
                      <?= $ciudad?>
                      <?php
                    }
                    ?>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nro.</th>
                                    <th>Identificador</th>
                                    <th>Total</th>
                                    <th>Total Modificado</th>
                                    <th>Fecha</th>
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nro.</th>
                                    <th>Identificador</th>
                                    <th>Total</th>
                                    <th>Total Modificado</th>
                                    <th>Fecha</th>
                                    <th>Accion</th>
                                </tr>
                            </tfoot>
                            <tbody>
                            <?php 
                            $database = new Database();
                            $db = $database->getConnection();
                            $reporte = new Reporte($db);
                            $stmt = $reporte->obtieneVentasPorRegionDetalle($vIdCiudad);
                            $num = $stmt->rowCount();
                            if($num != 0){
                                $indice = 0;
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                extract($row);
                                $indice = $indice + 1;
                                ?>
                                <tr>
                                    <th scope="row"><?= $indice?></th>
                                    <td><?= $identificador?></td>
                                    <td><?= $total?></td>
                                    <td><?= $totalModificado?></td>
                                    <td><?= $bitacora?></td>
                                    <td>
                                        <a class="btn btn-warning btn-sm" href="detalle_venta.php?id=<?php echo $idVenta?>">
                                            <i class="fas fa-eye"></i>
                                            Ver detalle
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="col-md-8">
                        Detalle Venta de productos por ciudad
                        <canvas id="GraficaDetalleProducto" width="400" height="400"></canvas>
                      </div>
                      <div class="col-md-2"></div>
                    </div>
                  </div>
                </div>
            <?php
            }
            ?>
          </section>
        </div>
      </div>
      <?php include("./includes/footer.php")?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include("./includes/scripts.php")?>
  <script src="operaciones/producto.js"></script>
  <script src="operaciones/reportes.js"></script>
  <script>
    let ListaCiudad=[];
    let ListaCiudadCantidad=[];
    <?php
      $database = new Database();
      $db = $database->getConnection();
      $reporte = new Reporte($db);
      $stmt = $reporte->obtieneVentasPorRegion();
      $num = $stmt->rowCount();
      if($num>0){
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
          extract($row);
          ?>
          ListaCiudad.push( '<?= $ciudad; ?>' );
          ListaCiudadCantidad.push( '<?= $ventas; ?>' );
          <?php
        }
      }
    ?>
    var ctx = document.getElementById('GraficaDetalleCiudad').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ListaCiudad,
            datasets: [{
                label: '# of Ventas',
                data: ListaCiudadCantidad,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
  </script>
  <script>
    let ListaProducto=[];
    let ListaCantidad=[];
    <?php
      $database = new Database();
      $db = $database->getConnection();
      $reporte = new Reporte($db);
      $stmt = $reporte->obtieneDetalleCantidadProductoPorRegion($vIdCiudad);
      $num = $stmt->rowCount();
      if($num>0){
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
          extract($row);
          ?>
          ListaProducto.push( '<?= $producto; ?>' );
          ListaCantidad.push( '<?= $cantidad; ?>' );
          <?php
        }
      }
    ?>
    var ctx = document.getElementById('GraficaDetalleProducto').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ListaProducto,
            datasets: [{
                label: '# productos',
                data: ListaCantidad,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
  </script>
</body>
</html>
