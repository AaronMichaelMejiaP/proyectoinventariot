<?php 
session_start();
if(!isset($_SESSION["Usuario"])) {
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./includes/header.php")?>
    <?php include("./includes/header_table.php")?>
</head>
<body id="page-top">
  <div id="wrapper">
    <?php include("./includes/slidebar.php")?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php include("./includes/navbar.php")?>
        <div class="container-fluid">
          <section>
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
          </section>
          <!-- CONTENIDO PAGINA -->
          <section>
            <h3>Cotizaciones</h3>
            <section>
              <div class="card shadow mb-1">
                <div class="card-body">
                <div class="d-flex bd-highlight">
                  <div class="bd-highlight">
                    <button type="button" class="btn btn-info">
                      <i class="fa fa-file" aria-hidden="true"></i>
                      Imprimir
                    </button>
                  </div>
                </div>
              </div>
            </section>
            <section>
            <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nro.</th>
                      <th>Identificador</th>
                      <th>Total</th>
                      <th>Total modificado</th>
                      <th>Fecha</th>
                      <th>Accion</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nro.</th>
                      <th>Identificador</th>
                      <th>Total</th>
                      <th>Total modificado</th>
                      <th>Fecha</th>
                      <th>Accion</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                  include_once 'dao/conexion/conexiondb.php';
                  include_once "dao/objects/venta.php";
                  $database = new Database();
                  $db = $database->getConnection();
                  $obj = new Venta($db);
                  $stmt = $obj->getCotizacion();
                  $num = $stmt->rowCount();
                  if($num != 0){
                    $indice = 0;
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $indice = $indice + 1;
                      ?>
                      <tr>
                        <th scope="row"><?= $indice?></th>
                        <td><?= $identificador?></td>
                        <td><?= $total?></td>
                        <td><?= $totalModificado?></td>
                        <td><?= $bitacora?></td>
                        <td>
                          <a class="btn btn-warning btn-sm" href="detalle_cotizacion.php?id=<?php echo $idVenta?>">
                            <i class="fas fa-eye"></i>
                            Ver detalle
                          </a>
                        </td>
                      </tr>
                      <?php
                    }
                  }
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          </section>
        </section>
        <!-- Modal -->
        <div class="modal fade" id="modalProducto" tabindex="-1" aria-labelledby="modalProductoLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalProductoLabel">Producto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div id="contenidoProducto">
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
      <?php include("./includes/footer.php")?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include("./includes/scripts.php")?>
  <?php include("./includes/scripts_table.php")?>

  <script src="operaciones/producto.js"></script>
</body>

</html>
