-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-12-2020 a las 17:17:17
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `deworld`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `idCiudad` int(11) NOT NULL,
  `ciudad` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`idCiudad`, `ciudad`, `descripcion`) VALUES
(1, 'LA PAZ', 'LA PAZ'),
(2, 'COCHABAMBA', 'COCHABAMBA'),
(3, 'SANTA CRUZ', 'SANTA CRUZ'),
(4, 'ORURO', 'ORURO'),
(5, 'TARIJA', 'TARIJA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `apellido` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ci` varchar(5000) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `correo` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `celular` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `fechaCreacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nombre`, `apellido`, `ci`, `correo`, `celular`, `fechaCreacion`) VALUES
(1, 'Daniel', 'Tola', '6108946LP', 'a@a.com', '75211456', '2020-12-09'),
(3, 'Juan', 'Perez', '65456446SZ', 'a@a.com', '75212355', '2020-12-15'),
(4, 'Ana', 'Banana', '897845CBBA', 'a@a.com', '7854213', '2020-12-15'),
(5, 'Jose', 'Perales', '6585254LP', 'jose@hotmail.com', '8546484', '2020-12-15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE `detalle` (
  `idDetalle` int(11) NOT NULL,
  `idVenta` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `precio` int(100) NOT NULL,
  `cantidad` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `detalle`
--

INSERT INTO `detalle` (`idDetalle`, `idVenta`, `idProducto`, `precio`, `cantidad`) VALUES
(1, 3, 1, 2300, 2),
(2, 3, 7, 80, 1),
(3, 4, 1, 2300, 2),
(4, 4, 7, 80, 1),
(5, 5, 1, 2300, 2),
(6, 5, 7, 80, 1),
(7, 6, 1, 2300, 2),
(8, 6, 7, 80, 1),
(9, 7, 1, 2351, 50),
(10, 7, 7, 2400, 30),
(11, 7, 8, 246, 3),
(12, 8, 1, 2500, 10),
(13, 9, 1, 2300, 2),
(14, 10, 1, 2300, 1),
(15, 11, 13, 450, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `idEstado` int(11) NOT NULL,
  `estado` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`idEstado`, `estado`, `descripcion`) VALUES
(1, 'ACTIVO', 'ACTIVO'),
(2, 'INACTIVO', 'INACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `idMarca` int(11) NOT NULL,
  `marca` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idMarca`, `marca`, `descripcion`) VALUES
(1, 'NIKE', 'Ropa y accesorios deportivos'),
(2, 'ADIDAS', 'Ropa y accesorios'),
(3, 'Lacost', 'perfumes'),
(4, 'TOSHIBA', 'computadoras'),
(5, 'LENOVO', 'Laptops'),
(6, 'PUMA', 'MARCA DEPORTIVA'),
(7, 'MORELIA', 'MARCA DEPORTIVA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idPersona` int(11) NOT NULL,
  `primerNombre` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `segundoNombre` varchar(500) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `primerApellido` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `segundoApellido` varchar(500) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `ci` int(100) NOT NULL,
  `expedido` varchar(11) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `telefono` int(100) DEFAULT NULL,
  `celular` int(100) DEFAULT NULL,
  `correo` varchar(500) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `direccion` varchar(500) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `zona` varchar(500) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `calle` varchar(500) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `numeroCasa` int(11) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idPersona`, `primerNombre`, `segundoNombre`, `primerApellido`, `segundoApellido`, `ci`, `expedido`, `telefono`, `celular`, `correo`, `direccion`, `zona`, `calle`, `numeroCasa`, `fechaNacimiento`) VALUES
(1, 'Aaron', 'Michael', 'Mejia', 'Pena', 6108943, 'LP', 2222222, 75213789, 'a@a.com', 'Miraflores;Guatemala;222;', 'Miraflores', 'Guatemala', 222, '1992-09-05'),
(2, 'Juan', 'Pablo', 'Guzman', '', 6545121, 'CBBA', 225548445, 75213789, 'a@a', 'Villa San Antonio;Burguez;2552;', 'Villa San Antonio', 'Burguez', 2552, '1999-05-05'),
(6, 'Sofia', 'Ana', 'Melgar', 'Mejia', 11111111, 'LP', 75551111, 75848484, 'a@a.com', 'mirflores;guatemala;222;', 'mirflores', 'guatemala', 222, '1992-09-05'),
(7, 'Jose', 'Pepe', 'Vaca', 'Quiroz', 615421, 'STC', 7585858, 75213154, 'a@a.com', 'miraflores;guatemala;test;', 'miraflores', 'guatemala', 0, '1900-10-05'),
(8, 'Samuel', 'Doria', 'Medina', 'Celi', 6108543, 'CBBA', 61075895, 75848454, 'a@a.com', 'San miguel;21;2521;', 'San miguel', '21', 2521, '1985-05-25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProducto` int(11) NOT NULL,
  `producto` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `codigo` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `precioCompra` float NOT NULL,
  `precioVenta` float NOT NULL,
  `preciosParaVenta` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cantidad` int(100) NOT NULL,
  `idMarca` int(11) NOT NULL,
  `idProveedor` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `idEstado` int(11) NOT NULL,
  `bitacora` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProducto`, `producto`, `codigo`, `precioCompra`, `precioVenta`, `preciosParaVenta`, `cantidad`, `idMarca`, `idProveedor`, `idTipo`, `idEstado`, `bitacora`) VALUES
(1, 'Mercurial Superfly', 'MRS-42', 1550.5, 2500.5, '2350.50;2300;2500;', 435, 2, 1, 1, 1, '2020-12-11'),
(7, 'Canguro', 'CG-L', 65, 100, '2350.50;2300;2400;', 769, 2, 2, 2, 1, '2020-08-13'),
(8, 'PREDATOR', 'PR-TR', 150, 250, '230;250;245.5;', 997, 2, 2, 1, 1, '2020-08-13'),
(9, 'COPA', 'CP-42', 200, 350, '330;340;360;', 0, 2, 2, 1, 1, '2020-08-13'),
(10, 'MAGISTA', 'NG-40', 200, 320, '330;335.50;300;', 500, 2, 2, 1, 2, '2020-08-13'),
(11, 'EVOPOWER', 'EVP-45', 150, 250, '250;280;', 500, 2, 2, 1, 1, '2020-08-16'),
(12, 'EVOPOWER', 'EVP-45', 150, 250, '250;280;', 500, 2, 2, 1, 2, '2020-08-16'),
(13, 'PREDATOR', 'PR-TR', 300, 350.5, '400.50;390.50;450;', 998, 2, 1, 1, 1, '2020-12-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idProveedor` int(11) NOT NULL,
  `proveedor` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `celular` int(11) NOT NULL,
  `correo` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `proveedor`, `descripcion`, `telefono`, `celular`, `correo`) VALUES
(1, 'COCHABAMBA', 'Proveedor de ropa', 0, 4452255, 'a@a.com'),
(2, 'COBIJAS SRL.', 'Prendas de ropa\r\n', 0, 6554545, 'a@a.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idRol` int(11) NOT NULL,
  `rol` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idRol`, `rol`, `descripcion`) VALUES
(1, 'ADMIN', 'ADMINISTRADOR'),
(2, 'USUARIO', 'USUARIO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `idTipo` int(11) NOT NULL,
  `tipo` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`idTipo`, `tipo`, `descripcion`) VALUES
(1, 'CALZADO', 'Calzado deportivo (FUTBOL)'),
(2, 'MATERIAL OFICINA', 'Varios materiales de oficina');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `usuario` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `pass` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `idRol` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `idEstado` int(11) NOT NULL,
  `idCiudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `usuario`, `pass`, `idRol`, `idPersona`, `idEstado`, `idCiudad`) VALUES
(1, '6108943LP', 'test', 1, 1, 1, 1),
(15, '6105454LP', '6105454LP', 2, 6, 1, 2),
(16, '6108543CBBA', '6108543CBBA', 2, 8, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idVenta` int(11) NOT NULL,
  `identificador` varchar(500) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `total` int(100) NOT NULL,
  `totalModificado` int(100) NOT NULL,
  `idTipoVenta` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `bitacora` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idVenta`, `identificador`, `total`, `totalModificado`, `idTipoVenta`, `idUsuario`, `bitacora`) VALUES
(4, '2222222LP', 4680, 4000, 2, 1, '2020-08-12'),
(5, '2222222LP', 4680, 4000, 2, 1, '2020-08-12'),
(6, '333333333CBBA', 4680, 4580, 1, 1, '2020-08-12'),
(7, '1111122222TEST', 190235, 190235, 1, 1, '2020-08-13'),
(8, '222222222LP', 25000, 25000, 1, 15, '2020-12-11'),
(9, '5652525', 4600, 4600, 1, 1, '2020-12-11'),
(10, '1', 2300, 2300, 1, 1, '2020-12-15'),
(11, '6108946LP', 900, 900, 1, 1, '2020-12-15');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`idCiudad`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD PRIMARY KEY (`idDetalle`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idEstado`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idMarca`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idTipo`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idVenta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `idCiudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `detalle`
--
ALTER TABLE `detalle`
  MODIFY `idDetalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `idMarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo`
--
ALTER TABLE `tipo`
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idVenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
