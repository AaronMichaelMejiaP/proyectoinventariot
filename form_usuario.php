
<?php 
session_start();
$vId = $_GET["id"];
if(!isset($_SESSION["Usuario"])) {
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./includes/header.php")?>
</head>
<body id="page-top">
  <div id="wrapper">
    <?php include("./includes/slidebar.php")?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php include("./includes/navbar.php")?>
        <div class="container-fluid">
          <section>
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
          </section>
          <!-- CONTENIDO PAGINA -->
          <section>
            <h3>Usuario</h3>
          </section>
          <section>
            <div class="card">
              <div class="card-body">
                <form action="dao/operaciones/usuario_crud.php" method="POST">
                  <?php
                  include_once 'dao/conexion/conexiondb.php';
                  include_once "dao/objects/persona.php";
                  include_once "dao/objects/usuario.php";
                  include_once "dao/objects/rol.php";
                  include_once "dao/objects/ciudad.php";
                  $database = new Database();
                  $db = $database->getConnection();

                  $objPersona = new Usuario($db);
                  $objPersona->idUsuario=$vId;
              
                  $stmt = $objPersona->getById();
                  $num = $stmt->rowCount();
                  if($num==1){
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    extract($row);
                    ?>
                    <h6>Datos personales</h6>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                          <label>Primer Nombre</label>
                          <input type="text" class="form-control" name="primerNombre" value="<?= $primerNombre?>" required>
                          <input type="hidden" name="idPersona" value="<?= $idPersona;?>">
                        </div>
                        <div class="col-md-6 mb-3">
                          <label>Segundo Nombre</label>
                          <input type="text" class="form-control" name="segundoNombre" value="<?= $segundoNombre?>" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                          <label>Primer Apellido</label>
                          <input type="text" class="form-control" name="primerApellido" value="<?= $primerApellido?>" required>
                        </div>
                        <div class="col-md-6 mb-3">
                          <label>Segundo Apellido</label>
                          <input type="text" class="form-control" name="segundoApellido" value="<?= $segundoApellido?>" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                          <label>Carnet</label>
                          <div class="input-group">
                            <input type="number" min="0" name="ci" value="<?= $ci?>" class="form-control">
                            <select class="custom-select" name="extension" required>
                              <option selected disabled>Extension...</option>
                              <?php
                              $opcionesExpedido = array("LP","CBBA","SC","OTRO");
                              foreach($opcionesExpedido as $data){
                                if($data == $expedido){
                                  $ComboSeleccionado = "selected";
                                }
                                else{
                                  $ComboSeleccionado = "";
                                }
                                ?>
                                <option value="<?php $data?>" <?= $ComboSeleccionado?>><?php echo $data;?></option>
                                <?php
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Correo</label>
                          <input type="email" class="form-control" name="correo" value="<?= $correo?>" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Fecha de nacimiento</label>
                          <input type="date" class="form-control" name="fechaNacimiento" value="<?= $fechaNacimiento?>" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label>Telefono</label>
                          <input type="number" min="0" class="form-control" name="telefono" value="<?= $telefono?>" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Celular</label>
                          <input type="number" min="0" class="form-control" name="celular" value="<?= $celular?>" required>
                        </div>
                    </div>
                    <hr>
                    <h6>Direccion</h6>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                          <label>Zona</label>
                          <input type="text" class="form-control" name="zona" value="<?= $zona?>" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Calle</label>
                          <input type="text" class="form-control" name="calle" value="<?= $calle?>" required>
                        </div>
                        <div class="col-md-4 mb-3">
                          <label>Nro.</label>
                          <input type="text" class="form-control" name="numeroCasa" value="<?= $numeroCasa?>" required>
                          <input type="text" class="form-control" name="idPersonaSeleccionada" value="TEST INPUT" required>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-warning" type="submit" name="persona_editar">Editar</button>
                    </div>
                    <?php
                  }
                  else{
                    ?>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                          <label>Personas</label>
                          <select class="custom-select" name="idPersonaSeleccionada" required>
                              <option selected disabled>Seleccione una opcion...</option>
                              <?php
                              $objPersona = new Persona($db);
                              $stmt = $objPersona->getSinUsuario();
                              $num = $stmt->rowCount();
                              if($num != 0){
                                $indice = 0;
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                extract($row);
                                $indice = $indice + 1;
                                ?>
                                <option value="<?php echo $idPersona?>"><?php echo $primerNombre." ".$primerApellido;?></option>
                                <?php
                                }
                              }
                              ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                          <label>Tipo usuario</label>
                          <select class="custom-select" name="idRolSeleccionado" required>
                              <option selected disabled>Seleccione una opcion...</option>
                              <?php
                              $objRol = new Rol($db);
                              $stmt = $objRol->get();
                              $num = $stmt->rowCount();
                              if($num != 0){
                                $indice = 0;
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                extract($row);
                                $indice = $indice + 1;
                                ?>
                                <option value="<?php echo $idRol?>"><?php echo $rol;?></option>
                                <?php
                                }
                              }
                              ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                          <label>Ciudad</label>
                          <select class="custom-select" name="idCiudadSeleccionada" required>
                              <option selected disabled>Seleccione una opcion...</option>
                              <?php
                              $objCiudad = new Ciudad($db);
                              $stmt = $objCiudad->get();
                              $num = $stmt->rowCount();
                              if($num != 0){
                                $indice = 0;
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                extract($row);
                                $indice = $indice + 1;
                                ?>
                                <option value="<?php echo $idCiudad?>"><?php echo $ciudad;?></option>
                                <?php
                                }
                              }
                              ?>
                          </select>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-success" type="submit" name="usuario_nuevo">Crear</button>
                    </div>
                    <?php
                  }
                  ?>
                    
                  </form>
              </div>
            </div>
          </section>
         
        </div>
      </div>
      <?php include("./includes/footer.php")?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include("./includes/scripts.php")?>

  <script src="operaciones/producto.js"></script>
</body>

</html>
