<?php 
session_start();
if(!isset($_SESSION["Usuario"])) {
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./includes/header.php")?>
</head>
<body id="page-top">
  <div id="wrapper">
    <?php include("./includes/slidebar.php")?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php include("./includes/navbar.php")?>
        <div class="container-fluid">
          <section>
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
          </section>
          <!-- CONTENIDO PAGINA -->
          <section>
            <h3>Administracion</h3>
            
            <div class="card">
              <ul class="nav nav-pills mb-3" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                  <a class="nav-link active" id="usuario-tab" data-toggle="tab" href="#usuario" role="tab" aria-controls="usuario" aria-selected="true">Usuarios</a>
                </li>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" id="proveedor-tab" data-toggle="tab" href="#proveedor" role="tab" aria-controls="proveedor" aria-selected="false">Proveedor</a>
                </li>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" id="marca-tab" data-toggle="tab" href="#marca" role="tab" aria-controls="marca" aria-selected="false" >Marca</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="usuario" role="tabpanel" aria-labelledby="usuario-tab">
                  <?php include("contenido_usuario.php")?>
                </div>
                <div class="tab-pane fade" id="proveedor" role="tabpanel" aria-labelledby="proveedor-tab">
                  <?php include("contenido_proveedor.php")?>
                </div>
                <div class="tab-pane fade" id="marca" role="tabpanel" aria-labelledby="marca-tab">
                  <?php include("contenido_marca.php")?>
                </div>
              </div>
            </div>
          </section>


        </div>
      </div>
      <?php include("./includes/footer.php")?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include("./includes/scripts.php")?>
  <script src="operaciones/producto.js"></script>
 
</body>

</html>
