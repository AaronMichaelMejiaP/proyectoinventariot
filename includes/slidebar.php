<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">DEWORLD</div>
  </a>

  <hr class="sidebar-divider my-0">

  <li class="nav-item active">
    <a class="nav-link" href="index.php">
    <i class="fas fa-home"></i>
    <span>Venta</span></a>
  </li>

  <?php if($_SESSION["IdRol"]==1) { ?>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
      Opciones
    </div>

    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProductos" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fab fa-product-hunt"></i>
        <span>Productos</span>
      </a>
      <div id="collapseProductos" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="inventario.php">Inventario</a>
          <a class="collapse-item" href="form_producto.php?id=0">Agregar producto</a>
        </div>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseVenta" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-border-all"></i>
        <span>Operaciones</span>
      </a>
      <div id="collapseVenta" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="ventas.php">Ventas</a>
          <a class="collapse-item" href="cotizaciones.php">Cotizaciones</a>
        </div>
      </div>
    </li>

    

    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Administracion
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePersonal" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-user-friends"></i>
        <span>Personal</span>
      </a>
      <div id="collapsePersonal" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="persona.php">Lista personas</a>
          <a class="collapse-item" href="form_persona.php?id=0">Agregar persona</a>
          <a class="collapse-item" href="usuario.php">Usuarios</a>
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdmin" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-cog"></i>
        <span>Administracion</span>
      </a>
      <div id="collapseAdmin" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="form_marca.php?id=0">Marcas</a>
          <!-- <a class="collapse-item" href="#">Proveedores</a>
          <a class="collapse-item" href="#">Tipo productos</a> -->
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReporte" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-cog"></i>
        <span>Reportes</span>
      </a>
      <div id="collapseReporte" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="reporte_ventas_ciudad.php?idCiudad=0">Ventas por ciudad</a>
          <!-- <a class="collapse-item" href="#">Proveedores</a>
          <a class="collapse-item" href="#">Tipo productos</a> -->
        </div>
      </div>
    </li>
  <?php } ?>
  <hr class="sidebar-divider d-none d-md-block">

  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
