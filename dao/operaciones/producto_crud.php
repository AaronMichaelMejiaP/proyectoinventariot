<?php
session_start();
include_once '../conexion/conexiondb.php';
include_once "../objects/producto.php";
$database = new Database();
$db = $database->getConnection();
$fecha = date(DATE_ATOM);
if (isset($_POST['producto_editar'])){
    if($_SESSION['IdRol'] == 1){
        $vIdProductoEditado=$_POST['idProducto'];
        $obj = new Producto($db);
        $obj->idProducto=$_POST['idProducto'];
        $obj->producto=$_POST['producto'];
        $obj->codigo=$_POST['codigo'];
        $obj->idMarca=$_POST['idMarca'];
        $obj->precioCompra=$_POST['precioCompra'];
        $obj->precioVenta=$_POST['precioVenta'];
        $obj->preciosParaVenta=$_POST['preciosRegistro'];
        $obj->cantidad=$_POST['cantidad'];
        $obj->idProveedor=$_POST['idProveedor'];
        $obj->idTipo=$_POST['idTipo'];
        $obj->idEstado=$_POST['idEstado'];
        $obj->bitacora=$fecha;

        if($obj->upDate()){
            $_SESSION["Mensaje"]="Se edito correctamente.";
            $_SESSION["MensajeTipo"]="success";
        }
        else{
            $_SESSION["Mensaje"]="No se pudo editar correctamente.";
            $_SESSION["MensajeTipo"]="danger";
        }
    }
    else{
        $_SESSION["Mensaje"]="No tiene autorizacion para esta operacion.";
        $_SESSION["MensajeTipo"]="danger";
    }
    header("location: ../../form_producto.php?id=$vIdProductoEditado");
}


if (isset($_POST['producto_nuevo'])){
    if($_SESSION['IdRol'] == 1){
        $obj = new Producto($db);
        $obj->producto=$_POST['producto'];
        $obj->codigo=$_POST['codigo'];
        $obj->idMarca=$_POST['idMarca'];
        $obj->precioCompra=$_POST['precioCompra'];
        $obj->precioVenta=$_POST['precioVenta'];
        $obj->preciosParaVenta=$_POST['preciosRegistro'];
        $obj->cantidad=$_POST['cantidad'];
        $obj->idProveedor=$_POST['idProveedor'];
        $obj->idTipo=$_POST['idTipo'];
        $obj->idEstado=1;
        $obj->bitacora=$fecha;

        if($obj->post()){
            $_SESSION["Mensaje"]="Se registro correctamente.";
            $_SESSION["MensajeTipo"]="success";
        }
        else{
            $_SESSION["Mensaje"]="No se pudo registrar correctamente.";
            $_SESSION["MensajeTipo"]="danger";
        }
    }
    else{
        $_SESSION["Mensaje"]="No tiene autorizacion para esta operacion.";
        $_SESSION["MensajeTipo"]="danger";
    }
    header("location: ../../inventario.php");
}
?>