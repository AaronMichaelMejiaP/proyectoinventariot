<?php
session_start();
include_once '../conexion/conexiondb.php';
include_once "../objects/usuario.php";
include_once "../objects/persona.php";
$database = new Database();
$db = $database->getConnection();
$fecha = date(DATE_ATOM);
if (isset($_POST['producto_editar'])){
    if($_SESSION['IdRol'] == 1){
        $vIdProductoEditado=$_POST['idProducto'];
        $obj = new Usuario($db);
        $obj->usuario=$_POST['usuario'];
        $obj->pass=$_POST['pass'];
        $obj->idRol=$_POST['idRol'];
        $obj->idPersona=$_POST['idPersona'];
        $obj->idEstado=1;
        
        if($obj->upDate()){
            $_SESSION["Mensaje"]="Se edito correctamente.";
            $_SESSION["MensajeTipo"]="success";
        }
        else{
            $_SESSION["Mensaje"]="No se pudo editar correctamente.";
            $_SESSION["MensajeTipo"]="danger";
        }
    }
    else{
        $_SESSION["Mensaje"]="No tiene autorizacion para esta operacion.";
        $_SESSION["MensajeTipo"]="danger";
    }
    header("location: ../../form_producto.php?id=$vIdProductoEditado");
}


if (isset($_POST['usuario_nuevo'])){
    if($_SESSION['IdRol'] == 1){
        $objPersona = new Persona($db);
        $objPersona->idPersona=$_POST['idPersonaSeleccionada'];
        $stmt = $objPersona->getById();
        $num = $stmt->rowCount();
        if($num==1){
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          extract($row);
          $obj = new Usuario($db);
          $obj->usuario= strval($ci.$expedido);
          $obj->pass=strval($ci.$expedido);
          $obj->idRol=$_POST['idRolSeleccionado'];
          $obj->idPersona=$_POST['idPersonaSeleccionada'];
          $obj->idCiudad=$_POST['idCiudadSeleccionada'];
          $obj->idEstado=1;
          if($obj->post()){
              $_SESSION["Mensaje"]="Se registro correctamente.";
              $_SESSION["MensajeTipo"]="success";
          }
          else{
              $_SESSION["Mensaje"]="No se pudo registrar correctamente.";
              $_SESSION["MensajeTipo"]="danger";
          }
        }
    }
    else{
        $_SESSION["Mensaje"]="No tiene autorizacion para esta operacion.";
        $_SESSION["MensajeTipo"]="danger";
    }
    header("location: ../../usuario.php");
}
?>