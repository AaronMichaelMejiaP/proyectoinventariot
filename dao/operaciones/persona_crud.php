<?php
session_start();
include_once '../conexion/conexiondb.php';
include_once "../objects/persona.php";
$database = new Database();
$db = $database->getConnection();
$fecha = date(DATE_ATOM);
if (isset($_POST['persona_editar'])){
    if($_SESSION['IdRol'] == 1){
        $vIdPersonaEditado=$_POST['idPersona'];
        $obj = new Persona($db);
        $obj->idPersona=$_POST['idPersona'];
        $obj->primerNombre=$_POST['primerNombre'];
        $obj->segundoNombre=$_POST['segundoNombre'];
        $obj->primerApellido=$_POST['primerApellido'];
        $obj->segundoApellido=$_POST['segundoApellido'];
        $obj->ci=$_POST['ci'].$_POST['extension'];
        $obj->telefono=$_POST['telefono'];
        $obj->celular=$_POST['celular'];
        $obj->correo=$_POST['correo'];
        $obj->direccion=$_POST['zona'].";".$_POST['calle'].";".$_POST['numeroCasa'].";";
        $obj->zona=$_POST['zona'];
        $obj->calle=$_POST['calle'];
        $obj->numeroCasa=$_POST['numeroCasa'];
        $obj->fechaNacimiento=$_POST['fechaNacimiento'];

        if($obj->upDate()){
            $_SESSION["Mensaje"]="Se edito correctamente.";
            $_SESSION["MensajeTipo"]="success";
        }
        else{
            $_SESSION["Mensaje"]="No se pudo editar correctamente.";
            $_SESSION["MensajeTipo"]="danger";
        }
    }
    else{
        $_SESSION["Mensaje"]="No tiene autorizacion para esta operacion.";
        $_SESSION["MensajeTipo"]="danger";
    }
    header("location: ../../form_persona.php?id=$vIdPersonaEditado");
}


if (isset($_POST['persona_nuevo'])){
    if($_SESSION['IdRol'] == 1){
        $obj = new Persona($db);
        $obj->primerNombre=strval( $_POST['primerNombre'] );
        $obj->segundoNombre=strval( $_POST['segundoNombre'] );
        $obj->primerApellido=strval( $_POST['primerApellido'] );
        $obj->segundoApellido=strval( $_POST['segundoApellido'] );
        $obj->ci=intval( $_POST['ci'] );
        $obj->expedido=strval( $_POST['extension'] );
        $obj->telefono=intval( $_POST['telefono'] );
        $obj->celular=intval( $_POST['celular'] );
        $obj->correo=strval( $_POST['correo'] );
        $obj->direccion=strval( $_POST['zona'].";".$_POST['calle'].";".$_POST['numeroCasa'].";" );
        $obj->zona=strval( $_POST['zona'] );
        $obj->calle=strval( $_POST['calle'] );
        $obj->numeroCasa=intval( $_POST['numeroCasa'] );
        $obj->fechaNacimiento=strval( $_POST['fechaNacimiento'] );

        if($obj->post()){
            $_SESSION["Mensaje"]="Se registro correctamente.";
            $_SESSION["MensajeTipo"]="success";
        }
        else{
            $_SESSION["Mensaje"]="No se pudo registrar correctamente.";
            $_SESSION["MensajeTipo"]="danger";
        }
    }
    else{
        $_SESSION["Mensaje"]="No tiene autorizacion para esta operacion.";
        $_SESSION["MensajeTipo"]="danger";
    }
    header("location: ../../persona.php");
}
?>



DATO>Sofia\n
DATO>Margarita\n
DATO>Guzman\n
DATO>Pena\n
DATO>23123123\n
DATO>LP\n
DATO>505050\n
DATO>606060\n
DATO>a@a.com\n
DATO>Miraflores;Guatemala;222;\n
DATO>Miraflores\n
DATO>Guatemala\n
DATO>222\n
DATO>2000-09-05\n