<?php 
session_start();  
include_once '../conexion/conexiondb.php';
include_once "../objects/venta.php";
include_once "../objects/detalle.php";
include_once "../objects/producto.php";

$database = new Database();
$db = $database->getConnection();

echo "COMPRA PRODUCTO";

if (isset($_POST['realizando_compra'])){
    echo "POST DETECTADO";
    $vIdUsuario=$_SESSION["IdUsuario"];
    $vTipo=$_POST['tipo'];
    $vIdentificador=$_POST['identificador'];
    $vTotal=$_POST['total'];
    $vTotalModificado=$_POST['totalModificado'];

    $fecha = date(DATE_ATOM);

    $obj = new Venta($db);

    $obj->identificador=$vIdentificador;
    $obj->total=$vTotal;
    $obj->totalModificado=$vTotalModificado;
    $obj->idTipoVenta=$vTipo;
    $obj->idUsuario=$vIdUsuario;
    $obj->bitacora=$fecha;

    if($obj->post()){

        $stmt = $obj->getLastRow();
        $num = $stmt->rowCount();
        if($num==1){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            extract($row);
            $ListaProductosSesion = json_decode($_SESSION["ListaProductos"]);
            $vCantidadProductos = count(json_decode($_SESSION["ListaProductos"]));
            $vCantidadSatisfactorio = 0;
            foreach($ListaProductosSesion as $data){
                json_decode($data->idProducto);
                json_decode($data->precio);
                json_decode($data->cantidad);
            
                $objDetalle = new Detalle($db);
                $objDetalle->idVenta=$id;
                $objDetalle->idProducto=json_decode($data->idProducto);
                $objDetalle->precio=json_decode($data->precio);
                $objDetalle->cantidad=json_decode($data->cantidad);
            
                if($objDetalle->post()){
                    $objProducto = new Producto($db);
                    $objProducto->idProducto=json_decode($data->idProducto);
                
                    $stmt = $objProducto->getById();
                    $num = $stmt->rowCount();
                
                    if($num==1){
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        extract($row);
                        $vNuevaCantidad = intval($cantidad) - intval(json_decode($data->cantidad));
                        $objProducto = new Producto($db);
                        $objProducto->idProducto=json_decode($data->idProducto);
                        $objProducto->cantidad=$vNuevaCantidad;

                        if($objProducto->upDateCantidad())
                        {
                            $vCantidadSatisfactorio = $vCantidadSatisfactorio +1;
                        }
                        else
                        {
                            $_SESSION["Mensaje"]="No se actualizo correctamente porfavor vuelva a intentarlo.";
                            $_SESSION["MensajeTipo"]="danger";
                        }
                    }
                }
            }
            if($vCantidadSatisfactorio == $vCantidadProductos){
                $_SESSION["Mensaje"]="Registro realizado correctamente.";
                $_SESSION["MensajeTipo"]="success";
                unset($_SESSION["ListaProductos"]);
            }
        }
        $obj = new Detalle($db);
    }
    else{
        $_SESSION["Mensaje"]="No se registro correctamente porfavor vuelva a intentarlo.";
        $_SESSION["MensajeTipo"]="danger";
    }
    header("location: ../../index.php");
}


?>