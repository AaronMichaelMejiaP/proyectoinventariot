<?php
session_start();
include_once '../conexion/conexiondb.php';
include_once "../objects/marca.php";
$database = new Database();
$db = $database->getConnection();
$fecha = date(DATE_ATOM);

if (isset($_POST['marca_nuevo'])){
    if($_SESSION['IdRol'] == 1){
        $obj = new Marca($db);
        $obj->marca = strval($_POST['marca']);
        $obj->descripcion = strval($_POST['descripcion']);
        if($obj->post()){
            $_SESSION["Mensaje"]="Se registro correctamente.";
            $_SESSION["MensajeTipo"]="success";
        }
        else{
            $_SESSION["Mensaje"]="No se pudo registrar correctamente.";
            $_SESSION["MensajeTipo"]="danger";
        }
    }
    else{
        $_SESSION["Mensaje"]="No tiene autorizacion para esta operacion.";
        $_SESSION["MensajeTipo"]="danger";
    }
    header("location: ../../form_marca.php?id=0");
}
?>