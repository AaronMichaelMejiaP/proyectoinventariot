<?php
session_start();
include_once '../conexion/conexiondb.php';
include_once "../objects/cliente.php";
$database = new Database();
$db = $database->getConnection();
$fecha = date(DATE_ATOM);
if (isset($_POST['cliente_nuevo'])){
    if($_SESSION['IdRol'] == 1){
        $obj = new Cliente($db);
        $obj->nombre=$_POST['nombre'];
        $obj->apellido=$_POST['apellido'];
        $obj->ci=$_POST['ci'];
        $obj->correo=$_POST['correo'];
        $obj->celular=$_POST['celular'];
        $obj->fechaCreacion=$fecha;
        if($obj->post()){
            $_SESSION["Mensaje"]="Se registro correctamente.";
            $_SESSION["MensajeTipo"]="success";
        }
        else{
            $_SESSION["Mensaje"]="No se pudo registrar correctamente.";
            $_SESSION["MensajeTipo"]="danger";
        }
    }
    else{
        $_SESSION["Mensaje"]="No tiene autorizacion para esta operacion.";
        $_SESSION["MensajeTipo"]="danger";
    }
    header("location: ../../form_cliente.php");
}
?>