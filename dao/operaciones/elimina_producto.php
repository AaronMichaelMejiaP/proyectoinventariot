<?php 
session_start();
$vId = $_GET["id"];
$ListaProductos = array();
$ListaProductosSesion = json_decode($_SESSION["ListaProductos"]);
foreach($ListaProductosSesion as $data){
    json_decode($data->idProducto);
    json_decode($data->precio);
    json_decode($data->cantidad);
    if($vId != json_decode($data->idProducto)){
        $Producto = [
            "idProducto" => json_decode($data->idProducto),
            "precio" => json_decode($data->precio),
            "cantidad" => json_decode($data->cantidad),
        ];
        array_push($ListaProductos, $Producto);
    }
}
$_SESSION['ListaProductos'] = json_encode($ListaProductos);
header("location: ../../compras.php");

?>