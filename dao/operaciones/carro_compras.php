<?php
session_start();
include_once '../conexion/conexiondb.php';
include_once "../objects/producto.php";

$database = new Database();
$db = $database->getConnection();
$ListaProductos = array();

if (isset($_POST['confirmacion_producto'])){
    $obj = new Producto($db);
    $obj->idProducto=$_POST['idProducto'];

    $stmt = $obj->getById();
    $num = $stmt->rowCount();

    if($num==1){
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        extract($row);
        if(intval($_POST['cantidad']) <= intval($cantidad)){
            if(isset($_SESSION["ListaProductos"])) {
                $ListaProductosSesion = json_decode($_SESSION["ListaProductos"]);
                foreach($ListaProductosSesion as $data){
                    $Producto = [
                        "idProducto" => json_decode($data->idProducto),
                        "precio" => json_decode($data->precio),
                        "cantidad" => json_decode($data->cantidad),
                    ];
                    array_push($ListaProductos, $Producto);
                }   
            }
            $Producto = [
                "idProducto" => $_POST['idProducto'],
                "precio" => $_POST['precio'],
                "cantidad" => $_POST['cantidad'],
            ];
            array_push($ListaProductos, $Producto);
            $_SESSION['ListaProductos'] = json_encode($ListaProductos);
            $_SESSION["Mensaje"]="Registro realizado correctamente.";
            $_SESSION["MensajeTipo"]="success";
        }
        else{
            $_SESSION["Mensaje"]="La cantidad que ingresaste no es valida.";
            $_SESSION["MensajeTipo"]="danger";
        }
    }
}

header("location: ../../index.php");

?>

