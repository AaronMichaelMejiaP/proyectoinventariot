<?php
class Usuario{
    // database connection and table modelo
    private $conn;
    private $table_modelo = "usuario";
 
    // object properties
    public $idUsuario;
    public $usuario;
    public $pass;
    public $idRol;
    public $idPersona;
    public $idEstado;

    public function __construct($db){
        $this->conn = $db;
    }
    function login(){
        $query = "
                SELECT
                    *
                FROM
                    " . $this->table_modelo . " 
                WHERE
                    usuario = ? and
                    pass = ?
                LIMIT
                    0,1
                    ";
     
        //$query;

        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->usuario);
        $stmt->bindParam(2, $this->pass);
        $stmt->execute();
     
        return $stmt;
    }

    // create user
    function post(){
        echo $this->usuario;
        echo $this->pass;
        //write query
        $query = "
                INSERT INTO
                    " . $this->table_modelo . "
                SET
                    usuario=?,
                    pass=?,
                    idRol=?,
                    idPersona=?,
                    idEstado=?,
                    idCiudad=?
                ";
 
                $stmt = $this->conn->prepare($query);
                
                $stmt->bindParam(1, $this->usuario);
                $stmt->bindParam(2, $this->pass);
                $stmt->bindParam(3, $this->idRol);
                $stmt->bindParam(4, $this->idPersona);
                $stmt->bindParam(5, $this->idEstado);
                $stmt->bindParam(6, $this->idCiudad);
              
                
        //echo $query;
 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
 
    }

    function get(){
        $consulta = "
        SELECT
            *
        FROM
            " . $this->table_modelo . " u
            INNER JOIN rol r
            ON u.idRol = r.idRol
            INNER JOIN estado e
            ON u.idEstado = e.idEstado
            INNER JOIN persona p
            ON u.idPersona = p.idPersona
        ";
    
        $query=$consulta;
     
        // echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
    }

    function getById(){
     
        $query = "
                SELECT
                    *
                FROM
                    " . $this->table_modelo . " 
                WHERE
                    idUsuario=?
                LIMIT
                    0,1
                    ";
     
        //echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->idUsuario);
        $stmt->execute();
     
        return $stmt;
    }
    function upDate(){
        $query = "
                UPDATE
                    " . $this->table_modelo . "
                SET
                    nombres=:nombres,
                    apellidos=:apellidos,
                    ci=:ci,
                    correo=:correo,
                    telefono=:telefono,
                    salario=:salario,
                    idArea=:idArea,
                    idCargo=:idCargo,
                    idEstado=:idEstado,
                    idCiudad=:idCiudad,
                    bitacora=:bitacora
                WHERE
                    idPersona=:idPersona
                    ";
     
        //echo $query;
        
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':nombres', $this->nombres);
        $stmt->bindParam(':apellidos', $this->apellidos);
        $stmt->bindParam(':ci', $this->ci);
        $stmt->bindParam(':correo', $this->correo);
        $stmt->bindParam(':telefono', $this->telefono);
        $stmt->bindParam(':salario', $this->salario);
        $stmt->bindParam(':idArea', $this->idArea);
        $stmt->bindParam(':idCargo', $this->idCargo);
        $stmt->bindParam(':idEstado', $this->idEstado);
        $stmt->bindParam(':idCiudad', $this->idCiudad);
        $stmt->bindParam(':bitacora', $this->bitacora);
        $stmt->bindParam(':idPersona', $this->idPersona);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    // delete the image
    function eliminar(){
        $query = "
        UPDATE
            " . $this->table_modelo . "
        SET
            idEstado=:idEstado
        WHERE
            idPersona=:idPersona
            ";

        //echo $query;

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':idEstado', $this->idEstado);
        $stmt->bindParam(':idPersona', $this->idPersona);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
       
    }
}
?>