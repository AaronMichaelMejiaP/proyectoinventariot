<?php
class Estado{
    // database connection and table modelo
    private $conn;
    private $table_modelo = "estado";
 
    // object properties
    public $idEstado;
    public $estado;
    public $descripcion;

    public function __construct($db){
        $this->conn = $db;
    }

    // create user
    function post(){

        //write query
        $query = "
                INSERT INTO
                    " . $this->table_modelo . "
                SET
                    idVenta=?,
                    idProducto=?,
                    precio=?,
                    cantidad=?
                ";
 
                $stmt = $this->conn->prepare($query);
                
                $stmt->bindParam(1, $this->idVenta);
                $stmt->bindParam(2, $this->idProducto);
                $stmt->bindParam(3, $this->precio);
                $stmt->bindParam(4, $this->cantidad);
              
                
        //echo $query;
 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
 
    }
    function get(){
        
        $consulta = "
        SELECT
            *
        FROM
            " . $this->table_modelo . " 
        ";
    
        $query=$consulta;
     
        // echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
    }
    function getById(){
     
        $query = "
                SELECT
                    *
                FROM
                    " . $this->table_modelo . " 
                WHERE
                    idProducto=?
                LIMIT
                    0,1
                    ";
     
        //echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->idProducto);
        $stmt->execute();
     
        return $stmt;
    }
    function Actializar(){
        $query = "
                UPDATE
                    " . $this->table_modelo . "
                SET
                    nombres=:nombres,
                    apellidos=:apellidos,
                    ci=:ci,
                    correo=:correo,
                    telefono=:telefono,
                    salario=:salario,
                    idArea=:idArea,
                    idCargo=:idCargo,
                    idEstado=:idEstado,
                    bitacora=:bitacora
                WHERE
                    idPersona=:idPersona
                    ";
     
        //echo $query;
        
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':nombres', $this->nombres);
        $stmt->bindParam(':apellidos', $this->apellidos);
        $stmt->bindParam(':ci', $this->ci);
        $stmt->bindParam(':correo', $this->correo);
        $stmt->bindParam(':telefono', $this->telefono);
        $stmt->bindParam(':salario', $this->salario);
        $stmt->bindParam(':idArea', $this->idArea);
        $stmt->bindParam(':idCargo', $this->idCargo);
        $stmt->bindParam(':idEstado', $this->idEstado);
        $stmt->bindParam(':bitacora', $this->bitacora);
        $stmt->bindParam(':idPersona', $this->idPersona);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    // delete the image
    function eliminar(){
        $query = "
        UPDATE
            " . $this->table_modelo . "
        SET
            idEstado=:idEstado
        WHERE
            idPersona=:idPersona
            ";

        //echo $query;

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':idEstado', $this->idEstado);
        $stmt->bindParam(':idPersona', $this->idPersona);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
       
    }
}
?>