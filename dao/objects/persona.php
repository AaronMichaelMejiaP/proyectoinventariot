<?php
class Persona{
    // database connection and table modelo
    private $conn;
    private $table_modelo = "persona";
    
    // object properties
    public $idPersona;
    public $primerNombre;
    public $segundoNombre;
    public $primerApellido;
    public $segundoApellido;
    public $ci;
    public $expedido;
    public $telefono;
    public $celular;
    public $correo;
    public $direccion;
    public $zona;
    public $calle;
    public $numeroCasa;
    public $fechaNacimiento;	

    public function __construct($db){
        $this->conn = $db;
    }
    // crea persona
    function post(){

        //write query
        $query = "
                INSERT INTO
                    " . $this->table_modelo . "
                SET
                    primerNombre=?,
                    segundoNombre=?,
                    primerApellido=?,
                    segundoApellido=?,
                    ci=?,
                    expedido=?,
                    telefono=?,
                    celular=?,
                    correo=?,
                    direccion=?,
                    zona=?,
                    calle=?,
                    numeroCasa=?,
                    fechaNacimiento=?
                ";
 
                $stmt = $this->conn->prepare($query);
                
                $stmt->bindParam(1, $this->primerNombre);
                $stmt->bindParam(2, $this->segundoNombre);
                $stmt->bindParam(3, $this->primerApellido);
                $stmt->bindParam(4, $this->segundoApellido);
                $stmt->bindParam(5, $this->ci);
                $stmt->bindParam(6, $this->expedido);
                $stmt->bindParam(7, $this->telefono);
                $stmt->bindParam(8, $this->celular);
                $stmt->bindParam(9, $this->correo);
                $stmt->bindParam(10, $this->direccion);
                $stmt->bindParam(11, $this->zona);
                $stmt->bindParam(12, $this->calle);
                $stmt->bindParam(13, $this->numeroCasa);
                $stmt->bindParam(14, $this->fechaNacimiento);
              
                
        //echo $query;
 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
 
    }
    
    function get(){
        
        $consulta = "
        SELECT
            *
        FROM
            " . $this->table_modelo . " 
        ";
    
        $query=$consulta;
     
        // echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
    }

    function getById(){
     
        $query = "
                SELECT
                    *
                FROM
                    " . $this->table_modelo . " 
                WHERE
                    idPersona=?
                LIMIT
                    0,1
                    ";
     
        //echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->idPersona);
        $stmt->execute();
     
        return $stmt;
    }
    function getByIdVenta(){
     
        $query = "
                SELECT
                    *, d.cantidad AS cantidadDetalle
                FROM
                    " . $this->table_modelo . " d
                    INNER JOIN productos p
                    ON p.idProducto = d.idProducto
                WHERE
                    idVenta=?
                    ";
     
        // echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->idVenta);
        $stmt->execute();
     
        return $stmt;
    }
    function upDate(){
        $query = "
                UPDATE
                    " . $this->table_modelo . "
                SET
                    primerNombre=:primerNombre,
                    segundoNombre=:segundoNombre,
                    primerApellido=:primerApellido,
                    segundoApellido=:segundoApellido,
                    ci=:ci,
                    expedido=:expedido,
                    telefono=:telefono,
                    celular=:celular,
                    correo=:correo,
                    direccion=:direccion,
                    zona=:zona,
                    calle=:calle,
                    numeroCasa=:numeroCasa,
                    fechaNacimiento=:fechaNacimiento
                WHERE
                    idPersona=:idPersona
                    ";
     
        //echo $query;
        
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':primerNombre', $this->primerNombre);
        $stmt->bindParam(':segundoNombre', $this->segundoNombre);
        $stmt->bindParam(':primerApellido', $this->primerApellido);
        $stmt->bindParam(':segundoApellido', $this->segundoApellido);
        $stmt->bindParam(':ci', $this->ci);
        $stmt->bindParam(':expedido', $this->expedido);
        $stmt->bindParam(':telefono', $this->telefono);
        $stmt->bindParam(':celular', $this->celular);
        $stmt->bindParam(':correo', $this->correo);
        $stmt->bindParam(':direccion', $this->direccion);
        $stmt->bindParam(':zona', $this->zona);
        $stmt->bindParam(':calle', $this->calle);
        $stmt->bindParam(':numeroCasa', $this->numeroCasa);
        $stmt->bindParam(':fechaNacimiento', $this->fechaNacimiento);
        $stmt->bindParam(':idPersona', $this->idPersona);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    function getSinUsuario(){
        $consulta = "
        SELECT *
        FROM persona
        WHERE 
        idPersona not in (
            SELECT idPersona
            FROM usuario
        )
        ";
    
        $query=$consulta;
     
        // echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
    }

    // delete the image
    function eliminar(){
        $query = "
        UPDATE
            " . $this->table_modelo . "
        SET
            idEstado=:idEstado
        WHERE
            idPersona=:idPersona
            ";

        //echo $query;

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':idEstado', $this->idEstado);
        $stmt->bindParam(':idPersona', $this->idPersona);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
       
    }
}
?>