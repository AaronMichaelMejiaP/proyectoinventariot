<?php
class Cliente{
    // database connection and table modelo
    private $conn;
    private $table_modelo = "cliente";
 
    // object properties
    public $idCliente;
    public $nombre;
    public $apellido;
    public $ci;
    public $correo;
    public $celular;
    public $fechaCreacion;
		
		
		

    public function __construct($db){
        $this->conn = $db;
    }

    // create user
    function post(){

        //write query
        $query = "
                INSERT INTO
                    " . $this->table_modelo . "
                SET
                    nombre=?,
                    apellido=?,
                    ci=?,
                    correo=?,
                    celular=?,
                    fechaCreacion=?
                ";
 
                $stmt = $this->conn->prepare($query);
                
                $stmt->bindParam(1, $this->nombre);
                $stmt->bindParam(2, $this->apellido);
                $stmt->bindParam(3, $this->ci);
                $stmt->bindParam(4, $this->correo);
                $stmt->bindParam(5, $this->celular);
                $stmt->bindParam(6, $this->fechaCreacion);
              
                
        //echo $query;
 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
 
    }

    function get(){
        $consulta = "
        SELECT
            *
        FROM
            " . $this->table_modelo . " 
        ";
    
        $query=$consulta;
     
        //echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
    }

    function getById(){
     
        $query = "
                SELECT
                    *
                FROM
                    " . $this->table_modelo . " 
                WHERE
                    idProducto=?
                LIMIT
                    0,1
                    ";
     
        //echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->idProducto);
        $stmt->execute();
     
        return $stmt;
    }
    function upDateCantidad(){
        $query = "
                UPDATE
                    " . $this->table_modelo . "
                SET
                    cantidad=:cantidad
                WHERE
                    idProducto=:idProducto
                    ";
     
        //echo $query;
        
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':cantidad', $this->cantidad);
        $stmt->bindParam(':idProducto', $this->idProducto);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    function upDate(){
        $query = "
                UPDATE
                    " . $this->table_modelo . "
                SET
                    producto=:producto,
                    codigo=:codigo,
                    precioCompra=:precioCompra,
                    precioVenta=:precioVenta,
                    preciosParaVenta=:preciosParaVenta,
                    cantidad=:cantidad,
                    idMarca=:idMarca,
                    idProveedor=:idProveedor,
                    idTipo=:idTipo,
                    idEstado=:idEstado,
                    bitacora=:bitacora
                WHERE
                    idProducto=:idProducto
                    ";
     
        //echo $query;
        
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':producto', $this->producto);
        $stmt->bindParam(':codigo', $this->codigo);
        $stmt->bindParam(':precioCompra', $this->precioCompra);
        $stmt->bindParam(':precioVenta', $this->precioVenta);
        $stmt->bindParam(':preciosParaVenta', $this->preciosParaVenta);
        $stmt->bindParam(':cantidad', $this->cantidad);
        $stmt->bindParam(':idMarca', $this->idMarca);
        $stmt->bindParam(':idProveedor', $this->idProveedor);
        $stmt->bindParam(':idTipo', $this->idTipo);
        $stmt->bindParam(':idEstado', $this->idEstado);
        $stmt->bindParam(':bitacora', $this->bitacora);
        $stmt->bindParam(':idProducto', $this->idProducto);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    // delete the image
    function eliminar(){
        $query = "
        UPDATE
            " . $this->table_modelo . "
        SET
            idEstado=:idEstado
        WHERE
            idPersona=:idPersona
            ";

        //echo $query;

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':idEstado', $this->idEstado);
        $stmt->bindParam(':idPersona', $this->idPersona);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
       
    }
}
?>