<?php
class Producto{
    // database connection and table modelo
    private $conn;
    private $table_modelo = "productos";
 
    // object properties
    public $idProducto;
    public $producto;
    public $codigo;
    public $precioCompra;
    public $precioVenta;
    public $preciosParaVenta;
    public $cantidad;
    public $idMarca;
    public $idProveedor;
    public $idTipo;
    public $idEstado;
    public $bitacora;

    public function __construct($db){
        $this->conn = $db;
    }

    // create user
    function post(){

        //write query
        $query = "
                INSERT INTO
                    " . $this->table_modelo . "
                SET
                    producto=?,
                    codigo=?,
                    precioCompra=?,
                    precioVenta=?,
                    preciosParaVenta=?,
                    cantidad=?,
                    idMarca=?,
                    idProveedor=?,
                    idTipo=?,
                    idEstado=?,
                    bitacora=?
                ";
 
                $stmt = $this->conn->prepare($query);
                
                $stmt->bindParam(1, $this->producto);
                $stmt->bindParam(2, $this->codigo);
                $stmt->bindParam(3, $this->precioCompra);
                $stmt->bindParam(4, $this->precioVenta);
                $stmt->bindParam(5, $this->preciosParaVenta);
                $stmt->bindParam(6, $this->cantidad);
                $stmt->bindParam(7, $this->idMarca);
                $stmt->bindParam(8, $this->idProveedor);
                $stmt->bindParam(9, $this->idTipo);
                $stmt->bindParam(10, $this->idEstado);
                $stmt->bindParam(11, $this->bitacora);
              
                
        echo $query;
 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
 
    }
    function get(){
        $consulta = "
        SELECT
            *
        FROM
            " . $this->table_modelo . " 
        ";
    
        $query=$consulta;
     
        // echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
    }
    function getById(){
     
        $query = "
                SELECT
                    *
                FROM
                    " . $this->table_modelo . " 
                WHERE
                    idProducto=?
                LIMIT
                    0,1
                    ";
     
        //echo $query;

        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->idProducto);
        $stmt->execute();
     
        return $stmt;
    }
    function upDateCantidad(){
        $query = "
                UPDATE
                    " . $this->table_modelo . "
                SET
                    cantidad=:cantidad
                WHERE
                    idProducto=:idProducto
                    ";
     
        //echo $query;
        
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':cantidad', $this->cantidad);
        $stmt->bindParam(':idProducto', $this->idProducto);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    function upDate(){
        $query = "
                UPDATE
                    " . $this->table_modelo . "
                SET
                    producto=:producto,
                    codigo=:codigo,
                    precioCompra=:precioCompra,
                    precioVenta=:precioVenta,
                    preciosParaVenta=:preciosParaVenta,
                    cantidad=:cantidad,
                    idMarca=:idMarca,
                    idProveedor=:idProveedor,
                    idTipo=:idTipo,
                    idEstado=:idEstado,
                    bitacora=:bitacora
                WHERE
                    idProducto=:idProducto
                    ";
     
        //echo $query;
        
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':producto', $this->producto);
        $stmt->bindParam(':codigo', $this->codigo);
        $stmt->bindParam(':precioCompra', $this->precioCompra);
        $stmt->bindParam(':precioVenta', $this->precioVenta);
        $stmt->bindParam(':preciosParaVenta', $this->preciosParaVenta);
        $stmt->bindParam(':cantidad', $this->cantidad);
        $stmt->bindParam(':idMarca', $this->idMarca);
        $stmt->bindParam(':idProveedor', $this->idProveedor);
        $stmt->bindParam(':idTipo', $this->idTipo);
        $stmt->bindParam(':idEstado', $this->idEstado);
        $stmt->bindParam(':bitacora', $this->bitacora);
        $stmt->bindParam(':idProducto', $this->idProducto);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    // delete the image
    function eliminar(){
        $query = "
        UPDATE
            " . $this->table_modelo . "
        SET
            idEstado=:idEstado
        WHERE
            idPersona=:idPersona
            ";

        //echo $query;

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':idEstado', $this->idEstado);
        $stmt->bindParam(':idPersona', $this->idPersona);

        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
       
    }
}
?>