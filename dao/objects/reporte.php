<?php
class Reporte{
    public function __construct($db){
        $this->conn = $db;
    }

    // create user
    function obtieneVentasPorRegion(){
        //write query
        $query = "
            SELECT c.idCiudad as idCiudad, c.ciudad as ciudad, COUNT(u.idCiudad) as ventas
            FROM usuario u , venta v , ciudad c
            WHERE u.idUsuario = v.idUsuario
            and c.idCiudad = u.idCiudad
            GROUP BY u.idCiudad
        ";
        
        // echo $query;
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
 
    }
    function obtieneVentasPorRegionDetalle($pIdCiudad){
        //write query
        $query = "
        SELECT c.idCiudad as idCiudad, c.ciudad as ciudad, v.*
        FROM usuario u , venta v , ciudad c
        WHERE u.idUsuario = v.idUsuario
        and c.idCiudad = u.idCiudad
        and c.idCiudad = ".$pIdCiudad."
        ";
        
        //echo $query;
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
 
    }
    function obtieneDetalleCantidadProductoPorRegion($pIdCiudad){
        //write query
        $query = "
        SELECT p.producto as producto, COUNT(d.idProducto) as cantidad 
        FROM usuario u , venta v , ciudad c, detalle d, productos p
        WHERE u.idUsuario = v.idUsuario
        and c.idCiudad = u.idCiudad
        and c.idCiudad = ".$pIdCiudad."
        and d.idVenta = v.idVenta
        and p.idProducto = d.idProducto
        GROUP BY d.idProducto
        ";
        
        //echo $query;
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
 
    }
    
}
?>