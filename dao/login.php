<?php
session_start();  
include_once 'conexion/conexiondb.php';
include_once "objects/usuario.php";

if (isset($_POST['verificando_login'])){
    $database = new Database();
    $db = $database->getConnection();
    
    $obj = new Usuario($db);
    $obj->usuario=$_POST['usuario'];
    $obj->pass=$_POST['pass'];
    $stmt = $obj->login();
    $num = $stmt->rowCount();
    
    if($num == 1){
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        extract($row);
        if($idEstado==1){
            $_SESSION['IdRol'] = $idRol;
            $_SESSION['Usuario'] = $usuario;
            $_SESSION['IdPersona'] = $idPersona;
            $_SESSION['IdUsuario'] = $idUsuario;
            
            header("location: ../index.php");
        }
        else{
            $_SESSION["Mensaje"]="El usuario se encuentra inactivo.";
            $_SESSION["MensajeTipo"]="danger";
            
            header("location: ../login.php");    
        }
    }
    else{
        $_SESSION["Mensaje"]="El usuario o la contraseña no son correctas intentelo nuevamente.";
        $_SESSION["MensajeTipo"]="danger";
        
        header("location: ../login.php");
    }
}
?>