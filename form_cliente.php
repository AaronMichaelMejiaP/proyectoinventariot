
<?php 
session_start();
if(!isset($_SESSION["Usuario"])) {
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./includes/header.php")?>
</head>
<body id="page-top">
  <div id="wrapper">
    <?php include("./includes/slidebar.php")?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php include("./includes/navbar.php")?>
        <div class="container-fluid">
          <section>
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <a class="btn btn-secondary" href="compras.php">Volver a la compra</a>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
          </section>
          <!-- CONTENIDO PAGINA -->
          <section>
            <h3>Marca</h3>
          </section>
          <section>
            <div class="card">
              <div class="card-body">
                <form action="dao/operaciones/cliente_crud.php" method="POST">
                  <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" name="nombre" placeholder="Ingrese un nombre...">
                  </div>
                  <div class="form-group">
                    <label for="">Apellido</label>
                    <input type="text" class="form-control" name="apellido" placeholder="Ingrese un apellido...">
                  </div>
                  <div class="form-group">
                    <label for="">Carnet / extension</label>
                    <input type="text" class="form-control" name="ci" placeholder="Ingrese un ci...">
                  </div>
                  <div class="form-group">
                    <label for="">Correo</label>
                    <input type="email" class="form-control" name="correo" placeholder="Ingrese un correo...">
                  </div>
                  <div class="form-group">
                    <label for="">Celular</label>
                    <input type="number" class="form-control" name="celular" placeholder="Ingrese un celular...">
                  </div>
                  <div class="d-flex justify-content-end">
                        <button class="btn btn-success" type="submit" name="cliente_nuevo">Crear</button>
                  </div>
                </form>
              </div>
            </div>
          </section>
         
        </div>
      </div>
      <?php include("./includes/footer.php")?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include("./includes/scripts.php")?>

  <script src="operaciones/producto.js"></script>
</body>

</html>
