function productoSeleccionadoCompra(pIdProducto, pPrecios, pCantidad){
    let vContenidoProducto = document.getElementById('contenidoProducto');
    let vPrecioEncontrado='';
    let vContenido=''
    for (let index = 0; index < pPrecios.length; index++) {
        const element = pPrecios[index];
        if(element != ';'){
            vPrecioEncontrado = vPrecioEncontrado + element;
        }
        else{
            vContenido = vContenido + `
            <div class="form-check">
                <input class="form-check-input" type="radio" name="precio" value="`+vPrecioEncontrado+`" required>
                <label class="form-check-label" for="exampleRadios1">
                `+vPrecioEncontrado+` Bs.
                </label>
            </div>
            `
            vPrecioEncontrado = '';
        }
    }
    
    vContenidoProducto.innerHTML = `
    <form class="user" action="dao/operaciones/carro_compras.php" method="POST">
        <div class="modal-body">
            <label>Precios</label>
            `+vContenido+`               
            <div class="form-group">
                <label>Cantidad</label>
                <input type="number" min="0" max="`+pCantidad+`" class="form-control" name="cantidad" placeholder="Ingresa la cantidad. . ." required>
                <input type="hidden" name="idProducto" value="`+pIdProducto+`">
            </div>  

        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
            <button type="submit" class="btn btn-primary" type="submit" name="confirmacion_producto">Comprar</button>
        </div>
    </form>
    `
}

let PreciosIngresados = [];
function registrarPrecio(){
    let vPrecioAgregado = document.getElementById('precioAgregado');
    PreciosIngresados.push(vPrecioAgregado.value);
    vPrecioAgregado.value = '';
    dibujaListaPrecios();
}

function dibujaListaPrecios(){
    let vPreciosIngresados = document.getElementById('preciosIngresados');
    let contenido = '';
    let index = 0;
    let preciosParaGuardar='';
    PreciosIngresados.forEach(precio => {
        contenido = contenido + `
        <li class="list-group-item d-flex justify-content-between align-items-center">
          `+precio+`
          <button class="btn btn-danger btn-sm" onclick="eliminarPrecioDeLista(`+index+`)">
            <i class="fa fa-trash" aria-hidden="true"></i>
          </button>
        </li>
        `  
        preciosParaGuardar=preciosParaGuardar+precio+';';
        index++;
    });
    contenido = contenido + `
    <input type="hidden" name="preciosRegistro" value="`+preciosParaGuardar+`">
    `

    vPreciosIngresados.innerHTML = contenido;
}

function registrarPrecioEditar(){ 
    let vPreciosIngresados = document.getElementById('preciosIngresados');
    let contenido = '';
    let index = 0;
    let preciosParaGuardar='';
    PreciosIngresados.forEach(precio => {
        contenido = contenido + `
        <li class="list-group-item d-flex justify-content-between align-items-center">
          `+precio+`
          <button class="btn btn-danger btn-sm" onclick="eliminarPrecioDeLista(`+index+`)">
            <i class="fa fa-trash" aria-hidden="true"></i>
          </button>
        </li>
        `  
        preciosParaGuardar=preciosParaGuardar+precio+';';
        index++;
    });
    
    contenido = contenido + `
    <input type="hidden" name="preciosRegistro" value="`+preciosParaGuardar+`">
    `
    vPreciosIngresados.innerHTML = contenido;
}

function editarPreciosRegistrados(pPrecios){
    let vContenidoPrincipalPrecios=document.getElementById('contenidoPrincipalPrecios');
    vContenidoPrincipalPrecios.innerHTML = `
    <div class="input-group mb-3">
      <input type="text" class="form-control" placeholder="Ingresa un precio" id="precioAgregado">
      <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button" onclick="registrarPrecio()">
          <i class="fas fa-plus"></i>  
          Agregar
          </button>
      </div>
    </div>
    `
    
    let vPrecioEncontrado='';
    for(let i=0;i<pPrecios.length;i++){
        if(pPrecios[i] != ';'){
            vPrecioEncontrado = vPrecioEncontrado + pPrecios[i];
        }
        else{
            PreciosIngresados.push(vPrecioEncontrado);
            vPrecioEncontrado='';
        }
    }

    dibujaListaPrecios();
}

function eliminarPrecioDeLista(pDato){
    PreciosIngresados.splice(pDato, 1);
    registrarPrecioEditar();
}

function personaSeleccionada(){
    let resp = document.getElementById('idPersona');
}