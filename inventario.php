<?php 
session_start();
if(!isset($_SESSION["Usuario"])) {
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./includes/header.php")?>
    <?php include("./includes/header_table.php")?>
</head>
<body id="page-top">
  <div id="wrapper">
    <?php include("./includes/slidebar.php")?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <?php include("./includes/navbar.php")?>
        <div class="container-fluid">
          <section>
            <?php if(isset($_SESSION["Mensaje"])) {?>
              <div class="alert alert-<?= $_SESSION["MensajeTipo"]?> alert-dismissible fade show" role="alert">
                <?= $_SESSION["Mensaje"];?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php unset($_SESSION["Mensaje"]); }?>
          </section>
          <!-- CONTENIDO PAGINA -->
          <h3>Inventario</h3>
          <section>
            <div class="card mb-1">
              <div class="card-body">
              <div class="d-flex bd-highlight">
                <div class="bd-highlight">
                  <a type="button" class="btn btn-success" href="form_producto.php?id=0">
                  <i class="fa fa-plus" aria-hidden="true"></i>
                    Agregar
                  </a>
                </div>
              </div>
            </div>
          </section>
         
          <section>
          <div class="card shadow mb-4">
           
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nro.</th>
                      <th>Producto</th>
                      <th>Codigo</th>
                      <th>Precio Compra</th>
                      <th>Precios</th>
                      <th>Cantidad</th>
                      <th>Estado</th>
                      <th>Accion</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nro.</th>
                      <th>Producto</th>
                      <th>Codigo</th>
                      <th>Precio Compra</th>
                      <th>Precios</th>
                      <th>Cantidad</th>
                      <th>Estado</th>
                      <th>Accion</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                    include_once 'dao/conexion/conexiondb.php';
                    include_once "dao/objects/producto.php";
                    $database = new Database();
                    $db = $database->getConnection();
                    $obj = new Producto($db);
                    $stmt = $obj->get();
                    $num = $stmt->rowCount();
                    if($num != 0){
                      $indice = 0;
                      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                      extract($row);
                      $indice = $indice + 1;
                      $EstadoCantidadProducto="";
                      if($cantidad == 0){
                        $EstadoCantidadProducto = "table-danger";
                      }
                      ?>
                      <tr class="<?php echo $EstadoCantidadProducto;?>">
                        <th scope="row"><?= $indice?></th>
                        <td><?= $producto?></td>
                        <td><?= $codigo?></td>
                        <td><?= $precioCompra?> Bs.</td>
                        <td>
                          <p>
                          <?php
                          $vPrecioOptenidoString="";
                          for ($x = 0; $x < strlen($preciosParaVenta) ; $x++) {
                            if($preciosParaVenta[$x] != ";"){
                              $vPrecioOptenidoString = $vPrecioOptenidoString.$preciosParaVenta[$x];
                            }
                            else{
                              ?>
                              
                              <?php echo $vPrecioOptenidoString ?> Bs.<br>
                              
                              <?php
                              $vPrecioOptenidoString = "";
                            }
                          }
                          ?>
                          </p>
                        </td>
                        <td><?= $cantidad?></td>
                        <td>
                          <?php
                          if($idEstado == 1){
                            ?>
                            <span class="badge badge-pill badge-success"><?php echo "ACTIVO"?></span>
                            <?php 
                          }
                          else{
                            ?>
                            <span class="badge badge-pill badge-danger"><?php echo "INACTIVO"?></span>
                            <?php 
                          }
                          ?>
                        </td>
                        <td>
                          <a class="btn btn-warning btn-sm" href="form_producto.php?id=<?php echo $idProducto?>">
                            <i class="fas fa-eye"></i>
                            Ver
                          </a>
                        </td>
                      </tr>
                      <?php
                      }
                    }
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          </section>
          <!-- Modal -->
          <div class="modal fade" id="modalProductoEditar" tabindex="-1" aria-labelledby="modalProductoEditarLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="modalProductoEditarLabel">Producto</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div id="contenidoProductoEditar">
                 
                </div>
              </div>
            </div>
          </div>
          <!-- Modal Compras -->
          <div class="modal fade" id="modalCompras" tabindex="-1" aria-labelledby="modalComprasLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="modalComprasLabel">Compras</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div id="contenidoCompras">
                    <?php
                    if(isset($_SESSION["ListaProductos"])) {
                      $ListaProductosSesion = json_decode($_SESSION["ListaProductos"]);
                      foreach($ListaProductosSesion as $data){
                          $Producto = [
                              "idProducto" => json_decode($data->idProducto),
                              "precio" => json_decode($data->precio),
                          ];
                          array_push($ListaProductos, $Producto);
                      }   
                    }
                    ?>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
                  <button type="button" class="btn btn-primary">Comprar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include("./includes/footer.php")?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <?php include("./includes/scripts.php")?>
  <?php include("./includes/scripts_table.php")?>
  
  
</body>

</html>
